# Sperm Cell Tracking Application

## Overview
This application presents a novel machine learning-based methodology designed for the automated detection and tracking of sperm cells within microscopic video recordings. The primary objective is to elucidate the dynamics and motion patterns of both individual sperm cells and sperm cell bundles.

## Methodology

The methodology employed in this study is meticulously structured into several sequential steps, each designed to address distinct aspects of sperm cell analysis through video recordings. This systematic approach enables comprehensive analysis from detection to classification and quantitative assessment. Below, we outline the key components of our methodology:

1. **Sperm Cell Detection**: Initial analysis focuses on isolating live sperm cells from the background. This process involves employing contour detection techniques to accurately identify and delineate individual sperm cells, subsequently surrounding each identified cell with a bounding box for further analysis.

2. **Path Reconstruction**: The trajectory of each sperm cell throughout the video sequence is reconstructed by examining the overlap of bounding boxes across consecutive frames. In instances where direct measurements are unavailable or measurements are noisy, the Kalman filter is applied to estimate the sperm cell's position, ensuring continuous tracking unencumbered by inaccurate measurements.

3. **Bounding Box Classification**: Upon successful detection and tracking, each bounding box is subjected to classification using a residual neural network. This classification categorizes the contents of each box into one of four distinct groups: a single sperm cell, a bundle of cells, a group of nearby cells, or other entities.

4. **Velocity Calculation**: With the paths of individual sperm cells established, we proceed to calculate key motility parameters such as the Straight-Line Velocity (VSL), the Average Path Velocity (VAP), and Curvilinear Velocity (VCL). These calculations are performed using the reconstructed paths and are essential for assessing the straightness and linearity of the cells' path.

5. **Final Analysis**: The culmination of this research involves a detailed evaluation of the collected data, applying a set of predefined rules to assess and interpret the behavior and characteristics of the sperm cells. This final step synthesizes the findings from the entire analysis, providing insights into sperm cell dynamics.

## Key Features
- Automated analysis of sperm motility and aggregation phenomena.
- Robust tool for researchers to study sperm behavior with enhanced accuracy and efficiency.

## Availability
- **Web-based User Interface**: A user-friendly web-based interface has been developed.
- **Latest Version**: The latest version of the program utilizing this methodology is publicly available at [Sperm Tracking Application](https://apps.datalab.fit.cvut.cz/sperm_tracking/).
- **Source Code**: Source code is publicly available on GitLab at [Sperm Cell Tracking App Repository](https://gitlab.fit.cvut.cz/horenjak/sperm_cell_tracking_app/).

## How to Use
- Simply visit the provided URL to access the web-based interface.
- Follow the instructions provided within the application for usage guidance.

